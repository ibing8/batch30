<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function table() {
        return view('contents.table', [
            "title" => "Table",
            "judul" => "Halaman Table"
        ]);
    }

    public function dataTables() {
        return view('contents.data-table', [
            "title" => "Data Tables",
            "judul" => "Halaman Data Tables"
        ]);
    }
}
