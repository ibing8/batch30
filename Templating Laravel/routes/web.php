<?php

use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TableController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('dashboard');

Route::get('/table', [TableController::class, 'table'])->name('table');

Route::get('/data-table', [TableController::class, 'dataTables'])->name('data.table');

/**
 * Create data table Cast
 */
Route::get('/cast/create', [CastController::class, 'create'])->name('cast.create');
Route::post('/cast', [CastController::class, 'store'])->name('cast.store');
/**
 * Get data tables Cast
 */
Route::get('/cast', [CastController::class, 'index'])->name('get.data.cast');
/**
 * Get data Cast by Id
 */
Route::get('/cast/{cast_id}', [CastController::class, 'show'])->name('cast.show');
/**
 * Update data table Cast
 */
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('cast.edit');
Route::put('/cast/{cast_id}', [CastController::class, 'update'])->name('cast.update');
/**
 * Delete data on table Cast
 */
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'])->name('cast.delete');

