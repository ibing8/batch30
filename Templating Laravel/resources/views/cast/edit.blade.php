@extends('layouts.index')

@section('judul')
    Edit Cast
@endsection

@section('button')
    <a href="{{ route('get.data.cast') }}" class="btn-sm btn-info">
        <span>Kembali</span>
    </a>
@endsection

@section('content')
    <form action="{{ route('cast.update', $cast->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" value="{{ $cast->nama }}" placeholder="Masukkan Nama Caster">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" name="umur" id="umur" value="{{ $cast->umur }}" placeholder="Masukkan Umur Caster">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" name="bio" id="bio" value="{{ $cast->bio }}" placeholder="Masukkan Bio Caster">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection
