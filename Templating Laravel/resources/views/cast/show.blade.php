@extends('layouts.index')\

@section('judul')
    Show Cast - {{ $cast->id }}
@endsection

@section('button')
    <a href="{{ route('get.data.cast') }}" class="btn-sm btn-info">
        <span>Kembali</span>
    </a>
@endsection

@section('content')
    <h4>Nama : {{ $cast->nama }}</h4>
    <p>Umur : {{ $cast->umur }}</p>
    <p>Bio : {{ $cast->bio }}</p>
@endsection
