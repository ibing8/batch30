<!DOCTYPE html>
<html>
    <head>
        <title>SaberBook</title>
        <style type="text/css">
            .header, .body {
                margin-bottom: 10px;
                font-size: large;
            }
        </style>
    </head>
<body>
    <div class="header">
        <h1>SELAMAT DATANG {{ $fName }} {{ $lName }}!</h1>
    </div>
    <div class="body">
        <h2>Terima Kasih telah bergabung di SaberBook. Social Media kita bersama!</h2>
    </div>

</body>
</html>
