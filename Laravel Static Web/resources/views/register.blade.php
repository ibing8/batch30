<!DOCTYPE html>
<html>
    <head>
        <title>Daftar Baru</title>
        <style type="text/css">
            .header, .body {
                margin-bottom: 10px;
                font-size: large;
            }

            .format, #fName, #lName, select {
                margin: 10px 0 10px 0;
            }

			#male, #lOne, textarea {
				margin-top: 10px;
			}
        </style>
    </head>
<body>
    <div class="header">
        <h1>Buat Account Baru!</h1>
    </div>
    <div class="body">
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <div class="format">
                <label for="fName">First name: </label>
                <br>
                <input type="text" name="fName" name="fName" placeholder="First Name">
                <br>
                <label for="lName">Last name: </label>
                <br>
                <input type="text" id="lName" name="lName" placeholder="Last Name">
            </div>
            <div class="format">
                <span>Gender: </span>
                <br>
                <input type="radio" class="gender" id="male" name="gender" value="Male">
                <label for="male">Male</label><br>
                <input type="radio" class="gender" id="female" name="gender" value="Female">
                <label for="female">Female</label><br>
                <input type="radio" class="gender" id="other" name="gender" value="Other">
                <label for="other">Other</label>
            </div>
            <div class="format">
                <span>Nationality: </span><br>
                <select name="nation">
                    <option value="Indonesia"/>Indonesia
                    <option value="Jerman"/>Jerman
                    <option value="Inggris"/>Inggris
                    <option value="China"/>China
                </select>
            </div>
            <div class="format">
                <span>Language Spoken: </span><br>
				<input type="checkbox" id="lOne" name="language" class="checkbox" value="Bahasa Indonesia">
				<label for="lOne">Bahasa Indonesia</label><br>
				<input type="checkbox" id="lTwo" name="language" class="checkbox" value="English">
				<label for="lTwo">English</label><br>
				<input type="checkbox" id="lTree" name="language" class="checkbox" value="Other">
				<label for="lTree">Other</label><br>
            </div>
            <div class="content3">
                <span>Bio: </span><br>
                <textarea rows="10" name="bio" placeholder="Bio"></textarea>
            </div>
            <div class="format">
                <input type="submit" value="Sign Up">
            </div>
        </form>
    </div>

</body>
</html>
