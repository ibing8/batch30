<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome(Request $req) {
        $fName = $req['fName'];
        $lName = $req['lName'];

        return view('welcome', compact('fName', 'lName'));
    }
}
